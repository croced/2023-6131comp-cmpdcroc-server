var express = require('express');
var router = express.Router();
var moment = require('moment');
var fs = require('fs');

const READINGS_PATH = 'readings.txt';

/* GET home page. */
router.get('/', function(req, res) {
  res.render('index', { title: '6131COMP Web Server' });
});

/* GET readings page. */
router.get('/readings', function(req, res) {
  const readings = fs.readFileSync(READINGS_PATH, 'utf-8').split('\n');
  res.render('readings', { title: '6131COMP Web Server - Readings', readings: readings });
});

router.get('/resetReadings', function(req, res) {
  fs.writeFile(READINGS_PATH, '', function(err) {
    if (err) throw err;
    console.log('File contents deleted');
  });
});

/**
 * Write a reading to the readings file
 * @param {*} reading in the format of group:temp:humidity
 */
const writeReading = (reading) => {
  let now = moment();
  let timeStamp = now.format('YYYY-MM-DD HH:mm:ss Z');

  const readingData = reading.split(':');

  const readingStr = `${timeStamp} - group ${readingData[0]} - temp: ${readingData[1]} - humidity: ${readingData[2]}\n`;
  console.log(readingStr);

  fs.appendFile(READINGS_PATH, readingStr, function (err) {
    if (err) throw err;
  });
}

/** 
 * POST a single reading
 */
router.post('/reading', function(req, res) {  
  const { reading } = req.body;

  if (!reading) {
    res.status(400).json('No readings provided');
    return;
  }

  writeReading(reading);
  res.status(200).json('OK');
});

/**
 * POST a batch of readings
 */
router.post('/readingBatch', function(req, res) {  
  const { readings } = req.body;

  console.log(req);

  if (!readings) {
    res.status(400).json('No readings provided');
    return;
  }

  const readingsArray = readings.split(',');
  
  // example reading: 1:22:50 (group, temp, humidity)
  // therefor an example batch: 1:22:50,2:23:51,3:24:52
  readingsArray.forEach(reading => {
    writeReading(reading);
  });

  res.status(200).json('OK');
});

module.exports = router;
